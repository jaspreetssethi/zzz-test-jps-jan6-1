package com.grainger.productcore.zzztestjpsjan61.data.account.provider;

import com.grainger.starter.boot.test.data.provider.GenericDataLoader;
import com.grainger.productcore.zzztestjpsjan61.data.account.model.AccountData;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("test-data.account")
public class AccountDataProperties extends GenericDataLoader<AccountData> {}
