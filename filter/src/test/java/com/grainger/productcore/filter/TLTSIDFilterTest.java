package com.grainger.starter.example.filter;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.MDC;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.util.concurrent.ListenableFuture;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

@RunWith(MockitoJUnitRunner.class)
public class TLTSIDFilterTest {

  private static final String TLTSID_HEADER = "TLTSID";

  @Mock HttpServletRequest httpRequest = new MockHttpServletRequest();

  @Mock ServletResponse servletResponse = new MockHttpServletResponse();

  @Mock FilterChain filterChain = new MockFilterChain();

  @InjectMocks TLTSIDFilter tltsidFilter = new TLTSIDFilter();

  @Test
  public void testTltsidVaule() {
    ((MockHttpServletRequest) this.httpRequest).addHeader(TLTSID_HEADER, "value");
    try {
      this.tltsidFilter.doFilter(this.httpRequest, this.servletResponse, this.filterChain);
      assertEquals("value", MDC.get(TLTSID_HEADER));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ServletException e) {
      e.printStackTrace();
    }
  }
}
