package com.grainger.starter.example.filter;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.*;

@Slf4j // lombok creates our logger as 'log' for us
@Component
public class TLTSIDFilter implements Filter {

  private static final String TLTSID_HEADER = "TLTSID";

  @Override
  public void destroy() {}

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
      throws java.io.IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    MDC.put(TLTSID_HEADER, httpRequest.getHeader(TLTSID_HEADER));
    filterChain.doFilter(request, response);
  }

  @Override
  public void init(FilterConfig filterconfig) throws ServletException {}
}
