# zzz-test-jps-jan6-1

Java API Starter from Template

## Getting started

Suppose we need a new service to support a new business line.
As a developer, I would like to be able to create a new service quickly, and have all of the common bits already done (for some definition of "common bits").

This is essentially what the starter kit provides: an opinionated starter skeleton which is capable of building a service and deploying it into production.

### Common Bits

At the current stage of development, the "common bits" are:

* Spring-Boot application structure
* Three-tier service architecture
    * API interface with SpringMVC and OpenAPI annotations
    * A Controller class implementing the API interface
    * A Service layer, consisting of:
        * Service Provider Interface (SPI)
        * Provider implementation
    * A Persistence layer
* Unit / Integration testing
* Architectural fitness functions (to enforce 3-tier arch)
* Flyway database migration support
* Code coverage metrics and minimum coverage thresholds
* Build execution time tracking
* Endpoint security via Open Policy Agent
* Kafka support
* Open Telemetry integration
* Gatling load / performance testing
* Model data lifecycle notifications (i.e. data mutation events)
* Documentation support, using MkDocs (deploy to Github Pages)
* OpenAPI v3.0 support for generating docs from code
* Versioning support for dependency updates
* Versioning support for building application
* Helm charts for deployment
* Container support
    * Docker container creation (app, db-init, opa-init)
* Local execution support
    * Docker-Compose definition for running service and dependencies
    * Supporting Postgres, Kafka, Jaeger, OPA, Spring-Boot app


## Verify the app is running

```bash
curl localhost:8081/actuator/health
curl localhost:8081/actuator/info
```

## Example endpoints

When the app starts, the database is created (migrated via Flyway) but not seeded - meaning the tables are set up, but not populated with data.
Running the Gatling load tests populates data in the table(s) by making POST requests to Create endpoints.

### Create a resource

```bash
curl -i -d '{
               "userName": "mary.q.contrary",
               "pii": "987-65-4321",
               "firstName": "Mary",
               "lastName": "Contrary"
       }' \
       -H 'Content-Type: application/json' \
       -X POST \
       localhost:8080/v1/example/facilityvisits
```

### Fetch a resource

```bash
curl -i localhost:8080/v1/example/facilityvisits/<id-from-previous-response>
```

### Fetch a list of all resources

```bash
curl localhost:8080/v1/example/facilityvisits
```

### Update a resource

```bash
curl -i -d '{
    "userName": "newUserName",
    "pii": "12356789",
    "firstName": "newFirstName",
    "lastName": "Contrary"
  }' \
  -H 'Content-Type: application/json' \
  -X PUT \
  localhost:8080/v1/example/facilityvisits/<id-from-previous-response>
```

### Delete a resource

```bash
curl -i -X DELETE localhost:8080/v1/example/facilityvisits/<id-from-previous-response>
```

## Load/Performance Tests

In order to see the system working, attach a consumer to the Kafka queue.
Then run the performance test to generate traffic.

```bash
scripts/consume-kafka.sh # <--- run this in a separate window
gradlew :app:gatlingRun  
```

The console output of the Gatling tests will provide a link to a browser view with details on the tests, e.g.

```text
Reports generated in 0s.
Please open the following file: /<path>/<to>/<project>/app/build/reports/gatling/<api-name>simulation-<timestamp>/index.html
== CSV Build Time Summary ==
Build time today: 6:50.347
Total build time: 6:50.347
(measured since 8 minutes ago)
```

## OpenAPI Spec

View the OpenAPI documentation for your service by navigating to [http://localhost:8080/swagger](http://localhost:8080/swagger) in your browser.

The links in the header section can be configured using the `starter.openapi` properties located in `/app/src/main/resources/application.yml`.

![Swagger UI Header Data](./images/swagger-ui-header-data.png "Swagger UI Header Data")

```bash
chrome http://localhost:8080/swagger
```

## Tracing

View the Jaeger monitoring system

```bash
chrome http://localhost:16686/
```
