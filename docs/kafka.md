# Kafka in the Java Starter Kit

This is a guide for using Kafka clients in the Java Starter Kit

The Java starter kit currently comes with both a producer and a consumer.

## What is Kafka?

Kafka is an event streaming platform that allows applications to publish and consume messages in real time. The benefits of Kafka are that it is fast, scalable, fault tolerant, and highly configurable.

### Prerequisite reading

* [Kafka Intro](https://kafka.apache.org/intro)
* [Kafka Key Concepts](https://kafka.apache.org/documentation/#gettingStarted)
* [Producers](https://docs.confluent.io/platform/current/clients/producer.html)
* [Consumers](https://docs.confluent.io/platform/current/clients/consumer.html)
* [Topics](https://developer.confluent.io/learn-kafka/apache-kafka/topics/)
* [Kafka Streams](https://kafka.apache.org/documentation/streams/)
* [Kafka Connect](https://docs.confluent.io/platform/current/connect/index.html)
* [Outbox pattern](https://microservices.io/patterns/data/transactional-outbox.html)

### Other resources

* [Kafka Tools](https://grainger.atlassian.net/wiki/spaces/PD/pages/48199244746/Kafka+Tools)
* [Kafka Components Information](https://grainger.atlassian.net/wiki/spaces/PD/pages/48188496824/Kafka+Components+Information)
* [pe-starterkit-kafka-topics](https://bitbucket.org/wwgrainger/pe-starterkit-kafka-topics/src/master/)

## Quick start for the Kafka Producer

A Kafka producer is responsible for publishing events to Kafka. To begin using the producer, you will first need to have a Kafka cluster to use. You can follow the [documentation on confluence that includes the information for the different Kafka components](https://grainger.atlassian.net/wiki/spaces/PD/pages/48188496824/Kafka+Components+Information) available from the PE team. If you are interested in Running kafka locally, there is currently a repository setup with instructions for [installing Kafka on Minikube](https://bitbucket.org/wwgrainger/productcore-kafka/src/master/).

After you have a Kafka cluster available, you will need to update the application.yml file with the appropriate configuration.

```yaml
  # Update the Kafka configuration for your respective Kafka cluster.
  kafka:
    bootstrap:
      url: BOOTSTRAP_URL:9092 # The bootstrap URL for the Kafka brokers.
    schema:
      registry:
        url: http://SCHEMA_REGISTRY_URL:8081 # The schema registry URL to read/write schemas 
# Look below in this doc for details on consumer
#   consumer:
#   ...
    producer:
      topic: TOPIC_NAME # The topic name for the producer to send messages to
      security:
        protocol: PLAINTEXT # PLAINTEXT or SSL
```

You can then build and deploy the application

```
# Retrieve CodeArtifact token for pulling dependencies
export CODEARTIFACT_AUTH_TOKEN=`aws-vault exec <team name>-prod -- aws codeartifact get-authorization-token --region us-east-2 --domain grainger --query authorizationToken --output text`

# Generate the Java class based on the Avro schema, and build the application JAR and the Docker image
./gradlew generateAvro bootJar
```

If you are deploying the application locally onto Kubernetes, you can then build the image and deploy using Helm

```
docker build -f Dockerfile.local -t test-skeleton:v1 .
helm upgrade --install kafka-producer deployment/helm --namespace default -f deployment/helm/values-local.yaml
```

You can then post messages to Kafka through the producer using the endpoint at `/publish/<some message>`

```
curl -X POST -H "Content-Type: application/json" http://<local_app_url>/publish/helloWorld
```
### Apache Avro

Avro provides the data serialization class for serializing the records when producing messages to Kafka. The benefits of Avro are that it is very fast and is schema-based. When sending messages to Kafka using Avro, the Avro schema also gets posted to the [Kafka Schema Registry](https://docs.confluent.io/platform/current/schema-registry/index.html). This allows the formats to be versioned and tracked. Versioning the message format is critical to protecting consumers from receiving incompatible message formats. To understand more about Avro, you can [check out the documentation](https://avro.apache.org/docs/current/).

The starter kit comes with a predefined Avro schema located at `data/src/main/resources/avro/Message.avsc`.

```json
{
  "namespace": "com.grainger.productcore.zzztestjpsjan61.data.producer.model",
  "type": "record",
  "name": "CHANGE_ME",
  "fields": [
      {"name": "message", "type": "string"}
  ]
}
```

New fields can be added to this schema. You can then generate a new Java class based for this using the `generateAvro` Gradle task. Check out the documentation to understand more about [Avro schemas](https://docs.oracle.com/cd/E26161_02/html/GettingStartedGuide/avroschemas.html).

```
# Retrieve CodeArtifact token for pulling dependencies
export CODEARTIFACT_AUTH_TOKEN=`aws-vault exec <team name>-prod -- aws codeartifact get-authorization-token --region us-east-2 --domain grainger --query authorizationToken --output text`

# Generate the Java class based on the Avro schema located at data/src/main/resources/avro/Message.avsc. This Java class represents the message format the producer will use when sending messages.
./gradlew generateAvro
```

### Outbox pattern

***This section is currently in development. The outbox pattern is going to be the recommended approach for producing messages to Kafka. There currently needs to be work to provide Kafka Connect instances to teams and a way to deploy their Connectors.***


## Quick start for the Kafka Consumer

A Kafka consumer is responsible for subscribing to events. To use the Kafka consumer, you will need to update the application.yml with the proper configuration.

```yaml
  # Update the Kafka configuration for your respective Kafka cluster.
    consumer:
      enabled: true # Conditional for if to run the KafkaListener for the consumer or not. Set to true to turn on consumer
      group:
        id: GROUP_ID # The group ID for the consumer. Consumers work in groups in order to consume from a topic in parallel. You can read more here https://docs.confluent.io/platform/current/clients/consumer.html#consumer-groups
      topic: TOPIC_NAME # The topic name for the consumer to subscribe to
      offset: earliest
      security:
        protocol: PLAINTEXT # PLAINTEXT or SSL
```

After updating the configuration, you can build and deploy the application. Be sure you have `CODEARTIFACT_AUTH_TOKEN` environment variable set

```
./gradlew bootJar
build -f Dockerfile.local -t test-skeleton .
helm upgrade --install kafka-producer deployment/helm --namespace default -f deployment/local_values.yaml
```

The consumer will log messages that it recieves. You can check the logs for a message like the following:

```
INFO 1 --- [ntainer#0-0-C-1] i.t.s.e.c.service.ConsumerService : Message received by consumer: {"message": "helloWorld"}
```

### Consumer Groups

Consumers can work together to consume events from a topic through what are called [consumer groups](https://docs.confluent.io/platform/current/clients/consumer.html#consumer-groups). These groups share a group ID, and then divide the topic partitions as fairly amongst themselves as possible by establishing that each partition is only consumed by a single consumer from the group. Consumers groups and the number of replicas for a consumer should therefore be considered in conjunction with the number of partitions a topic has.

### Offset Management

An [offset](https://docs.confluent.io/platform/current/clients/consumer.html#offset-management) is a way in which the consumer keeps track where in the topics message queue a consumer has left off reading from. When a consumer is done processing a list of messages, it will commit the offset back to Kafka to let the rest of the consumers know where in the queue to pick up from. Offsets are important for ensuring that consumers can work together as a group without duplicating work.

## Deploying Topics

Platform Engineering provides the Kafka infrastructure to use. This includes the Kafka brokers, the Schema Registry, and AKHQ. These resources are listed on Confluence on the [Kafka Components Information page](https://grainger.atlassian.net/wiki/spaces/PD/pages/48188496824/Kafka+Components+Information). They also provide a [starter kit for deploying new topics to the MSK clusters](https://bitbucket.org/wwgrainger/pe-starterkit-kafka-topics/src/master/).

## Using kafka Connect

Kafka Connect is a service that provides the ability to connect an external data source to Kafka. When deploying Kafka locally and running the producer locally, you can hook up the Postgresql database to Kafka Connect.

You first need to change some of the configuration properties for Postgresql so that it uses logical write-ahead logging. You can overwrite the configuration for Postgresql through the helm flag `db.conf_override=true`

```
helm upgrade --install kafka-producer deployment/helm --namespace default --set db.conf_override=true -f deployment/helm/values-local.yaml
```

After the app and database have been deployed, you will need to set up a new connector in Kafka Connect.

A new connector is created simply through a configuration file. The [productcore-kafka repository](https://bitbucket.org/wwgrainger/productcore-kafka/src/master/) currently has instructions for deploying Kafka Connect with a new connector using the Strimzi KafkaConnector CRD for local development.

Within the values file where the list of connectors are defined, you will need the include the following connector:

```yaml
  connectors:
    - name: debezium-postgresql-connector # name for the Connector
      class: io.debezium.connector.postgresql.PostgresConnector # plugin class to use for the connector
      config:
        plugin.name: "pgoutput"
        database.hostname: "CHANGE_ME" # the hostname for the database connection. You will need to change this to the Service name for the DB deployment, e.g. my-app-starterkit-api.default.svc.cluster.local
        database.port: "5432" # port number for the DB
        database.user: "postgresadmin" # username for the DB connection
        database.password: "admin123" # password for the DB connection
        database.dbname: "postgres" # database for the Connector to watch. A Connector will need to be setup for each DB you wish to connect
        database.server.name: "fulfillment" # this is a unique name that gets prepended to the topic names when Kafka Connect sets up a topic. Each table in the DB gets its own topic, and this name will be prepended. So the format is <database.server.name>.<table name>
        table.include.list: "public.kafkadata,public.ToDoItem" # list of tables in the DB for the Connector to monitor.

```

Follow the instructions in that repo to deploy Kafka Connect and the connector.

With Kafka Connect and the connector running, any changes to the database tables will be relayed to Kafka.

```
curl -X POST -H "Content-Type: application/json" http://<local_app_url>/todo -d '{"title": "this is a title", "done": true}'
```

## Kafka Streams

***This section is currently in development***
