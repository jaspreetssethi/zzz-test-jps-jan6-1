{{- if eq .Values.db.type "local" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "zzz-test-jps-jan6-1-db.fullname" . }}
  labels:
    {{- include "zzz-test-jps-jan6-1-db.labels" . | nindent 4 }}
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "zzz-test-jps-jan6-1-db.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "zzz-test-jps-jan6-1-db.labels" . | nindent 8 }}
    spec:
      containers:
        - name: postgres
          image: postgres:11.4
          imagePullPolicy: "IfNotPresent"
          ports:
            - containerPort: 5432
          envFrom:
            - configMapRef:
                name: {{ include "zzz-test-jps-jan6-1-db.fullname" . }}
          {{- if .Values.db.conf_override }}
          args:
            - -c
            - 'config_file=/etc/postgresql/postgresql.conf'
          volumeMounts:
            - mountPath: /etc/postgresql/postgresql.conf
              subPath: postgresql.conf
              name: postgresql-conf
          {{- end }}
      {{- if .Values.db.conf_override }}
      volumes:
        - name: postgresql-conf
          configMap:
            name: {{ include "zzz-test-jps-jan6-1-db.fullname" . }}-conf
      {{- end }}
{{- end }}
