{{- if eq .Values.db.type "local" -}}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "zzz-test-jps-jan6-1-db.fullname" . }}
  labels:
    {{- include "zzz-test-jps-jan6-1.labels" . | nindent 4 }}
data:
  POSTGRES_DB: postgres
  POSTGRES_USER: postgresadmin
  POSTGRES_PASSWORD: admin123
{{- end }}
