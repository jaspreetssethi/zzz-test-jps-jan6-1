{{/*
Expand the name of the chart.
*/}}
{{- define "zzz-test-jps-jan6-1.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "zzz-test-jps-jan6-1.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "zzz-test-jps-jan6-1.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "zzz-test-jps-jan6-1.labels" -}}
helm.sh/chart: {{ include "zzz-test-jps-jan6-1.chart" . }}
app: {{ include "zzz-test-jps-jan6-1.name" . }}
version: {{ default .Chart.AppVersion .Values.tag }}
{{ include "zzz-test-jps-jan6-1.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- include "zzz-test-jps-jan6-1.graingerLabels" . }}
{{- end }}

{{- define "zzz-test-jps-jan6-1-db.labels" -}}
helm.sh/chart: {{ include "zzz-test-jps-jan6-1.chart" . }}
app: {{ include "zzz-test-jps-jan6-1.name" . }}
version: {{ default .Chart.AppVersion .Values.tag }}
{{ include "zzz-test-jps-jan6-1-db.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- include "zzz-test-jps-jan6-1.graingerLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "zzz-test-jps-jan6-1.selectorLabels" -}}
app.kubernetes.io/name: {{ include "zzz-test-jps-jan6-1.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "zzz-test-jps-jan6-1-db.selectorLabels" -}}
app.kubernetes.io/name: {{ include "zzz-test-jps-jan6-1.name" . }}-db
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Grainger labels
*/}}
{{- define "zzz-test-jps-jan6-1.graingerLabels" -}}
{{- if .Values.teamname }}
grainger.com/managed-by-team: {{ .Values.teamname }}
{{- end -}}
{{- if .Values.reponame }}
grainger.com/repo: {{ .Values.reponame }}
{{- end -}}
{{- end -}}

{{/*
Image Repository
*/}}
{{- define "zzz-test-jps-jan6-1.container" -}}
{{- if .depObj.image.registry }}
    {{- .depObj.image.registry -}}
    /
{{- end -}}
{{- .depObj.image.repository -}}
:
{{- .depObj.image.tag | default .Chart.AppVersion }}
{{- end -}}

{{/*
Expand the name of the chart.
*/}}
{{- define "zzz-test-jps-jan6-1-db.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 60 | trimSuffix "-" }}-db
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "zzz-test-jps-jan6-1-db.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 60 | trimSuffix "-" }}-db
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 60 | trimSuffix "-" }}-db
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 60 | trimSuffix "-" }}-db
{{- end }}
{{- end }}
{{- end }}

{{ define "zzz-test-jps-jan6-1-db.dbServiceHost" -}}
{{- if eq .Values.db.type "local" -}}
{{- include "zzz-test-jps-jan6-1-db.fullname" . -}}
{{- else -}}
{{- .Values.db.serviceHost -}}
{{- end -}}
{{- end }}

{{ define "zzz-test-jps-jan6-1.productionSlot" -}}
{{- if eq .Values.productionSlot "blue" -}}
blue
{{- else -}}
green
{{- end -}}
{{- end }}

{{ define "zzz-test-jps-jan6-1.testSlot" -}}
{{- if eq .Values.productionSlot "blue" -}}
green
{{- else -}}
blue
{{- end -}}
{{- end }}
