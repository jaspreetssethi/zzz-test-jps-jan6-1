{{- if eq .Values.db.type "local" -}}
apiVersion: v1
kind: Service
metadata:
  name: {{ include "zzz-test-jps-jan6-1-db.fullname" . }}
  labels:
    {{- include "zzz-test-jps-jan6-1-db.labels" . | nindent 4 }}
spec:
  type: NodePort
  selector:
    {{- include "zzz-test-jps-jan6-1-db.selectorLabels" . | nindent 4 }}
  ports:
    - name: "tcp-broker-port"
      port: {{ .Values.db.port }}
      targetPort: 5432
{{- end }}
