{{- if .Values.gateway.enabled }}
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: {{ include "zzz-test-jps-jan6-1.fullname" . }}
  labels:
    {{- include "zzz-test-jps-jan6-1.labels" . | nindent 4 }}
spec:
  hosts:
    - {{ .Values.fqdn }}
  gateways:
    - {{ include "zzz-test-jps-jan6-1.fullname" . }}
  http:
    {{ if and .Values.blue.enabled .Values.green.enabled }}
    - name: test
      match:
        - uri:
            prefix: "/test"
      rewrite:
        uri: "/"
      route:
        - destination:
            port:
              number: 80
            host: {{ include "zzz-test-jps-jan6-1.fullname" . }}
            subset: test
    {{ end }}
    - name: prod
      route:
        - destination:
            port:
              number: 80
            host: {{ include "zzz-test-jps-jan6-1.fullname" . }}
            {{ if and .Values.blue.enabled .Values.green.enabled }}
            subset: prod
            {{ end }}

{{- end }}