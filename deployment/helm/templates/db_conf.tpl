{{- if and (eq .Values.db.type "local") (.Values.db.conf_override) -}}
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "zzz-test-jps-jan6-1-db.fullname" . }}-conf
  labels:
    {{- include "zzz-test-jps-jan6-1.labels" . | nindent 4 }}
data:
  "postgresql.conf": |
    listen_addresses='*'
    port=5432
    wal_level=logical
    max_wal_senders=1
    max_connections=1000
    shared_buffers=512MB
    fsync=off
    synchronous_commit=off
    full_page_writes=off
{{- end }}
