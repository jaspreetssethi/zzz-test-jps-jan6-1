{{ if and .Values.blue.enabled .Values.green.enabled }}
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: {{ include "zzz-test-jps-jan6-1.fullname" . }}
spec:
  host: {{ include "zzz-test-jps-jan6-1.fullname" . }}
  subsets:
    - name: prod
      labels:
        slot: {{ include "zzz-test-jps-jan6-1.productionSlot" . }}
    - name: test
      labels:
        slot: {{ include "zzz-test-jps-jan6-1.testSlot" . }}
{{ end }}
