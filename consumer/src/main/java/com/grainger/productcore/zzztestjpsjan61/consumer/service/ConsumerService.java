package com.grainger.productcore.zzztestjpsjan61.consumer.service;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.concurrent.CountDownLatch;

@Slf4j // lombok creates our logger as 'log' for us
@Service
public class ConsumerService {

    private CountDownLatch latch = new CountDownLatch(100);

    public void processMessage(ConsumerRecord<String, Object> record) {
        try {
            log.info("Message received by consumer: " + record.value().toString());
            latch.countDown();
        } catch (Exception e) {
            log.error("There was an error reading the record: " + e.getMessage());
        }
    }

    public CountDownLatch getLatch() {
      return this.latch;
    }
}
