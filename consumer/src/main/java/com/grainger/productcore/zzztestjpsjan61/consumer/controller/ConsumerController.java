package com.grainger.productcore.zzztestjpsjan61.consumer.controller;

import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import org.springframework.kafka.annotation.KafkaListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import com.grainger.productcore.zzztestjpsjan61.consumer.service.ConsumerService;

@ConditionalOnExpression("${spring.kafka.consumer.enabled:true}")
@Component
public class ConsumerController {
    /*
     * Create a simple consumer that reads messages from a topic
     */

    @Autowired
    private ConsumerService consumerService;

    @KafkaListener(topics = "#{'${spring.kafka.consumer.topic}'}")
    public void processMessage(ConsumerRecord<String, Object> record) {
      consumerService.processMessage(record);
    }
}
