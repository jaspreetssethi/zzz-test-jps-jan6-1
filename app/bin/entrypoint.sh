#!/bin/sh
# prepare the full URL based on the hostname from AWS

exec java -jar \
  -Dspring.profiles.active=local \
  -Djava.security.egd=file:/dev/urandom \
  $JAVA_PROFILE \
  $JAVA_OPTS \
  -DDEPLOYMENT_TYPE=$DEPLOYMENT_TYPE \
  /usr/local/bin/app-0.1.0-SNAPSHOT.jar \
