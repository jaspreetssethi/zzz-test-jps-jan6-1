package com.grainger.productcore.zzztestjpsjan61.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.grainger.productcore.zzztestjpsjan61.persistence.model.ToDoItem;
import com.grainger.productcore.zzztestjpsjan61.service.provider.ToDoService;

@WebMvcTest(ToDoController.class)
public class ToDoControllerTest {
    
    @Autowired
    private MockMvc MockMvc;

    @MockBean
    private ToDoService toDoService;

    private ToDoItem toDoItem;

    @BeforeEach
    void setUp() {

        toDoItem = ToDoItem.builder()
                            .id(1L)
                            .title("To Do Assignment")
                            .done(false)
                            .build();
    }

    @Test
    void saveToDoItem() throws Exception {

        ToDoItem inputToDoItem = ToDoItem.builder()
                                    .title("To Do Assignment")
                                    .done(false)
                                    .build();
        
        Mockito.when(toDoService.save(inputToDoItem)).thenReturn(toDoItem);

        MockMvc.perform(MockMvcRequestBuilders.post("/todo").contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                        "    \"title\" : \"To Do Assignment\",\n" +
                        "    \"done\" : \"false\"\n" +
                        "}"))
            .andExpect(MockMvcResultMatchers.status().isOk());

    }
}
