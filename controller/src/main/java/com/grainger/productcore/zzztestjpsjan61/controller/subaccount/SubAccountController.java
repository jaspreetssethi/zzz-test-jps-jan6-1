// package com.grainger.productcore.zzztestjpsjan61.controller.subaccount;

// import com.grainger.starter.boot.exception.RequestValidationException;
// import com.grainger.starter.boot.exception.ResourceNotFoundException;
// import com.grainger.starter.boot.notifier.lifecycle.entity.spi.EntityLifecycleNotifier;
// import com.grainger.productcore.zzztestjpsjan61.api.responses.PagedResponse;
// import com.grainger.productcore.zzztestjpsjan61.api.subaccount.requests.SubAccountRequest;
// import com.grainger.productcore.zzztestjpsjan61.api.subaccount.resources.SubAccountResource;
// import com.grainger.productcore.zzztestjpsjan61.api.subaccount.responses.SubAccountResponse;
// import com.grainger.productcore.zzztestjpsjan61.controller.subaccount.mapper.SubAccountRequestMapper;
// import com.grainger.productcore.zzztestjpsjan61.service.spi.subaccount.SubAccountService;
// import com.grainger.productcore.zzztestjpsjan61.service.spi.subaccount.model.SubAccount;
// import lombok.extern.slf4j.Slf4j;
// import org.springframework.data.domain.Page;
// import org.springframework.data.domain.Pageable;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.web.bind.annotation.RestController;

// import java.net.URI;
// import java.util.Optional;

// @Slf4j
// public class SubAccountController implements SubAccountResource {

//   private final SubAccountService manager;
//   private final SubAccountRequestMapper mapper;
//   private final EntityLifecycleNotifier notifier;
//   // TODO: Need to find a better way to determine version of entity
//   private final String entityVersion = "0.0.1";

//   /**
//    * constructor.
//    *
//    * @param manager instance of SubAccount manager
//    * @param mapper instance of SubAccount request mappper
//    */
//   public SubAccountController(
//       SubAccountService manager, SubAccountRequestMapper mapper, EntityLifecycleNotifier notifier) {
//     this.manager = manager;
//     this.mapper = mapper;
//     this.notifier = notifier;
//   }

//   @Override
//   public ResponseEntity<SubAccountResponse> addEntity(SubAccountRequest addEntityRequest)
//       throws RequestValidationException {

//     log.info("username->{}", addEntityRequest.getUserName());
//     SubAccount resource = mapper.toModel(addEntityRequest);
//     SubAccount saved = manager.add(resource);
//     SubAccountResponse response = mapper.toSubAccountResponse(saved);
//     notifier.created(saved, entityVersion, URI.create("user:anonymous"));
//     return new ResponseEntity<>(response, HttpStatus.CREATED);
//   }

//   @Override
//   public ResponseEntity<SubAccountResponse> findEntityById(String id)
//       throws ResourceNotFoundException {

//     log.info("id->{}", id);
//     Optional<SubAccount> found = manager.findById(id);
//     return new ResponseEntity<>(
//         found
//             .map(r -> mapper.toSubAccountResponse(r))
//             .orElseThrow(() -> new ResourceNotFoundException(id)),
//         HttpStatus.OK);
//   }

//   @Override
//   public ResponseEntity<PagedResponse<SubAccountResponse>> findEntities(Pageable pageable) {
//     Page<SubAccount> resources = manager.findAll(pageable);

//     return new ResponseEntity<>(mapper.toSubAccountResponsePage(resources), HttpStatus.OK);
//   }

//   @Override
//   public ResponseEntity<SubAccountResponse> updateEntityById(String id, SubAccountRequest request)
//       throws ResourceNotFoundException, RequestValidationException {

//     log.info("id->{}", id);
//     Optional<SubAccount> found = manager.updateById(id, mapper.toModel(request));
//     if (found.isPresent()) {
//       notifier.updated(found.get(), entityVersion, URI.create("user:anonymous"));
//     }
//     return new ResponseEntity<>(
//         found
//             .map(r -> mapper.toSubAccountResponse(r))
//             .orElseThrow(() -> new ResourceNotFoundException(id)),
//         HttpStatus.OK);
//   }

//   @Override
//   public ResponseEntity<SubAccountResponse> deleteEntityById(String id)
//       throws ResourceNotFoundException {

//     log.info("id->{}", id);
//     Optional<SubAccount> found = manager.deleteById(id);
//     if (found.isPresent()) {
//       notifier.deleted(found.get(), entityVersion, URI.create("user:anonymous"));
//     }
//     return new ResponseEntity<>(
//         found
//             .map(r -> mapper.toSubAccountResponse(r))
//             .orElseThrow(() -> new ResourceNotFoundException(id)),
//         HttpStatus.OK);
//   }
// }
