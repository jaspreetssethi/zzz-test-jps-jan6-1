package com.grainger.productcore.zzztestjpsjan61.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController  
public class HelloWorldController   {
    @Value("${DEPLOYMENT_TYPE:}")
    private String deploymentType;

    @GetMapping("/")  
    public String hello()   {
        log.info("Log inside the / api to test the MDC.");
        return (deploymentType != null && !deploymentType.equals("")) ? "Hello World " + deploymentType : "Hello World!";
    }  

    @PostMapping("/")  
    public String hello1()   {
        return "Hello World!";
    }
}
