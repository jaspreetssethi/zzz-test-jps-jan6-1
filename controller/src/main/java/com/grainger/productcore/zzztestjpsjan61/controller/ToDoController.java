package com.grainger.productcore.zzztestjpsjan61.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.grainger.productcore.zzztestjpsjan61.service.provider.ToDoService;
import com.grainger.productcore.zzztestjpsjan61.persistence.model.ToDoItem;


@RestController
@RequestMapping("/todo")
public class ToDoController {
    
    @Autowired
    private ToDoService toDoService;

    @GetMapping
    public Iterable<ToDoItem> findAll() {

        return toDoService.findAll();
    }

    @PostMapping
    public ToDoItem save(@RequestBody ToDoItem toDoItem) {

        return toDoService.save(toDoItem);
    }
}
