package com.grainger.productcore.zzztestjpsjan61.persistence.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import java.util.ArrayList;
import java.util.List;


@DataJpaTest
public class ToDoItemRepositoryTest {
    
    @Autowired
    private ToDoItemRepository toDoItemRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    ToDoItem inputToDoItemOne;
    ToDoItem inputToDoItemTwo;

    @BeforeEach
    void setUp() {

        inputToDoItemOne = ToDoItem.builder()
                                    .title("To Do Assignment One")
                                    .done(false)
                                    .build();
        
        testEntityManager.persist(inputToDoItemOne);

        inputToDoItemTwo = ToDoItem.builder()
                                    .title("To Do Assignment Two")
                                    .done(false)
                                    .build();
        
        testEntityManager.persist(inputToDoItemTwo);
    }

    @Test
    public void findToDoItems() {

        Iterable<ToDoItem> toDoItemList = toDoItemRepository.findAll();
        List<ToDoItem> expectedList = new ArrayList<>();
        expectedList.add(inputToDoItemOne);
        expectedList.add(inputToDoItemTwo);
        assertEquals(expectedList, toDoItemList);
    }

}
