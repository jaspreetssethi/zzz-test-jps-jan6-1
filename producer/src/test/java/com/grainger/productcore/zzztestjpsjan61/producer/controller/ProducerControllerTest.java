package com.grainger.productcore.zzztestjpsjan61.producer.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.grainger.productcore.zzztestjpsjan61.producer.service.ProducerService;

@WebMvcTest(ProducerController.class)
public class ProducerControllerTest {
    
    @Autowired
    private MockMvc MockMvc;

    @MockBean
    private ProducerService producerService;

    @Test
    void sendMessage() throws Exception {
        String message = "ThisIsATest";
        String success = "Published successfully!";
        
        Mockito.when(producerService.sendMessage(message)).thenReturn(success);

        MockMvc.perform(MockMvcRequestBuilders.post("/publish/" + message)).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
