package com.grainger.productcore.zzztestjpsjan61.producer.service;

import org.apache.kafka.clients.producer.*;
import org.springframework.kafka.core.KafkaTemplate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.grainger.productcore.zzztestjpsjan61.data.producer.model.Message;

@Service
public class ProducerService {

    @Value("${spring.kafka.producer.topic}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, Message> producer;

    public String sendMessage(String m) {
        Message message = new Message(m);
        ProducerRecord<String, Message> record = new ProducerRecord<>(topic, "key", message);
        producer.send(record);
        return "Published successfully!";
    }
}
