package com.grainger.productcore.zzztestjpsjan61.service.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grainger.productcore.zzztestjpsjan61.persistence.model.ToDoItem;
import com.grainger.productcore.zzztestjpsjan61.persistence.model.ToDoItemRepository;

@Service
public class ToDoService {
    
    @Autowired
    private ToDoItemRepository toDoItemRepository;


    public Iterable<ToDoItem> findAll() {
        
        return toDoItemRepository.findAll();
    }


    public ToDoItem save(ToDoItem toDoItem) {

        return toDoItemRepository.save(toDoItem);
    }

}
