package com.grainger.productcore.zzztestjpsjan61.service.provider;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.grainger.productcore.zzztestjpsjan61.persistence.model.ToDoItem;
import com.grainger.productcore.zzztestjpsjan61.persistence.model.ToDoItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest(classes = {ToDoService.class})
public class ToDoServiceTest {
    
    @Autowired
    private ToDoService toDoService;

    @MockBean
    private ToDoItemRepository toDoItemRepository;

    static ToDoItem requestToDoItem;

    @BeforeAll
    public static void init() {
         requestToDoItem = ToDoItem.builder()
                .id(1L)
                .title("To Do Assignment")
                .done(true)
                .build();
    }

    @BeforeEach
    void setUp() {

        ToDoItem responseToDoItem = ToDoItem.builder()
                                    .id(1L)
                                    .title("To Do Assignment")
                                    .done(true)
                                    .build();
        
        Mockito.when(toDoItemRepository.save(requestToDoItem))
                .thenReturn(responseToDoItem);
    }

    @Test
    public void save() {

        ToDoItem responseToDoItem = toDoService.save(requestToDoItem);
        assertEquals(requestToDoItem, responseToDoItem);

    }
}
